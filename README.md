Entering Open Source Hardware – an Introduction
===

## General

This module teaches the basics of open source hardware.

Find a summarising module description [here](module-description.md) and a more detailed description below.

Slides rely on [reveal.js](https://revealjs.com/) and are written in markdown.

To launch them in a presentable mode just copy the markdown source into a new document of any instance of HedgeDoc (e.g. [this one](https://demo.hedgedoc.org/)).


## In Class

Find the slides here:

- [slide show](https://md.opensourceecology.de/p/ZP0XqorC3)
- [source](slides/S-Intro-OSH.md)

### Structure

**roughly:**

- Intro to the concept of OSH
- give the vision
- are there already projects covering your idea? → congrats! that's an advantage
- how open are these projects?
- pitch

**detailled:**

- Intro & storytelling (15min)
- IP Block (patents are dumb) (15min)
- Q&A (5min)

optional _Break_ (10min)

- What's OSH? (10min)
- G - Unlock → ideas where openness makes sense for your invention (30min)
- OSH in practice + business models (15min)
- Q&A (5min)

_Coffee Break_ (20min)

- How to open the source + where to search for OSH? (10min)
- G - technology research (40min)
- G - openness check (15min)

_Break_ (10min)

- G - pitch (25min)
- Outro (10min)

_Buffer = 5_

**time plan:**

| block          | time | time cumulated | hour     |
|----------------|------|----------------|----------|
| -              | 0    | 0              | 14:00:00 |
| Intro          | 15   | 15             | 14:15:00 |
| IP             | 15   | 30             | 14:30:00 |
| _Q&A_          | 5    | 35             | 14:35:00 |
| **Break**      | 10   | 45             | 14:45:00 |
| OSH Intro      | 10   | 55             | 14:55:00 |
| G - Unlock     | 30   | 85             | 15:25:00 |
| OSH Practice   | 15   | 100            | 15:40:00 |
| _Q&A_          | 5    | 105            | 15:45:00 |
| **Break**      | 20   | 125            | 16:05:00 |
| HowTo Open     | 10   | 135            | 16:15:00 |
| G - Technology | 40   | 175            | 16:55:00 |
| G - Openness   | 15   | 190            | 17:10:00 |
| **Break**      | 10   | 200            | 17:20:00 |
| _Pitch_        | 25   | 225            | 17:45:00 |
| Outro          | 10   | 235            | 17:55:00 |

## Contributing

### Point of contact

Giving feedback or contributing to the material directly e.g. via merge request is always highly appreciated. This is what keeps the thing rolling.

In case you want to get in touch with the core team of maintainers, for the moment the easiest would be to simply drop an issue in which you explain your motivation for the contact. Happy to e-meet you then :)

### File name convention

It's fairly easy:

[type]-[module]-[sub-module]

S – Slides
G – Group Action

EXAMPLE 1:

`S-Intro-OSH.md` = slides for the Introduction module, specifically covering OSH

EXAMPLE 2:

`G-deep-value.md` = description for group action within the [Deep Dive module](#deep-dive), specifically the value creation exercise
