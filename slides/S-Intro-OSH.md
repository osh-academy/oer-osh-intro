---
title: Entering Open Source Hardware (OSH at SRH 04-21)
type: slide
slideOptions:
  transition: slide
...

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

## Entering
## Open Source Hardware
---

These are Open Educational Resources (OER)
used under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

[source](https://gitlab.com/osh-academy/oer-osh-intro/-/blob/master/slides/S-Intro-OSH.md)

<!--- 240min in total -->

---

## Who's Martin Häuer?

<img src="https://greennetproject.org/wp-content/uploads/2020/06/33.jpg" style="border: none;background: none;box-shadow:none" height="300">


- researcher & activist for OSH
- focus on infrastructure
- chairman of Open Source Ecology Germany e.V. (non-profit)

---

<iframe width="1024" height="576" src="https://media.ccc.de/v/36c3-oio-192-open-source-hardware-in-industry-meet-din-spec-3105/oembed" frameborder="0" allowfullscreen></iframe>

---

## Today's Schedule
---

1. The Conventional Way
2. The Open Source Way
3. Your Entry Point (+ pitch)

Note:
    - First I'm gonna show you the conventional way innovation was managed
    - Then Open Source
    - We'll conclude with you finding your maybe first touch points with the community. And of course you'll pitch your new solutions to the rest of the course

---

## A Short Story on Innovative Entrepreneurship

The Case of Gardena

<img src="https://www.golem.de/1911/144722-213324-213323_rc.jpg" style="border: none;background: none;box-shadow:none" height="350">

Note:
    source: https://www.golem.de/news/gardena-open-source-wie-es-sein-soll-1911-144722.html

---

## A Short Story on Cooperation (Game Theory)

Simulation:

- virus infesting mammals
- virus mutates

<span>**Q:**<!-- .element: class="fragment" data-fragment-index="1" --></span><span> Will virus tribes compete or collaborate? <!-- .element: class="fragment" data-fragment-index="1" --></span>

<span>**A:**<!-- .element: class="fragment" data-fragment-index="1" --></span> <span>Depends on the population of mammals.<!-- .element: class="fragment" data-fragment-index="2" --></span>

Note:
    for Q&A: patents are a powerful tool to conquer market shares in a scarce field

---

# IP Law
---

in times of open source

Note:
    Anyone knows what 'IP' stands for?
    → Intellectual Property


---

essentially: 2 mutually exclusive fields
(from our perspective)

## Copyright

## Patent law

## <span>+ Trademarks<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

## Copyright
---

…gives the owner the exclusive right to use and to make copies of a <span><!-- .element: class="fragment highlight-red" data-fragment-index="2" -->creative</span> work.

<span>--
    Official 'sharing' of your work requires a license.<!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    [Drawing action](lecture-actions/L-deep-osh-draw.md)
    A license is sort of a standard contract with users.
    So, you want to broadcast my movie? Pay this fee and stick with those rules and everything will be fine.


---

"[no one can copyright an]
idea, procedure, process, system, method of operation, concept, principle, or discovery."

    Copyright Act, Section 102(b)

Note:
    For that we have… you guess it →


---

## Patent
---
…gives the owner the exclusive right to make, use or sell an invention […].

<span>--
    …shareable by a license.<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

### How to distinguish products from different manufacturers?

---

## Trademark™
---

…identifies products/services of a particular source.

|      |      |             |      |
| ---- | ---- | ----------- | ---- |
| name | logo | catchphrase | etc. |

<span>OSH action figures can be built by anyone,
    but only the trademark holder
    can sell Hulk (or whoever).<!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    So let's get more practical →

---

## Old world says:

> If you have a great idea:
> Go and patent it!

---

<!-- .slide: data-transition="fade" -->

<img class="r-stretch" src="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/forbes-screenshot.png">

([article](https://www.forbes.com/sites/stephenkey/2017/11/13/in-todays-market-do-patents-even-matter/#4e2c9d2c56f3))

---

<!-- .slide: data-transition="fade" -->

## How is that?

Note: [Discussion] 1…5min

---

- inventions must be novel & nonobvious
- patents are costly
- patenting takes a while

---

> "Everything that can be invented has been invented."

    Henry L. Ellsworth, commissioner of the U.S. Patent Office,
    1843 report to Congress

<!--- source:
https://en.wikiquote.org/wiki/Patent#Misattributed
-->

Note:
    Well, we all know that's not true - especially in 1843.
    But since patentable ideas must be _new_ without infringing an existent patent…

---

After almost 550 years of patent law

<img src="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/noun_Missing Puzzle Piece_1445716.svg" style="border: none;background: white;box-shadow:none" height="400">

it's hard to find patentable gaps.

<span>+ keep in mind patents aren't for free<!-- .element: class="fragment" data-fragment-index="1" --></span>

<!--- source:
https://en.wikipedia.org/wiki/History_of_patent_law#cite_note-1
-->

---

<span>filing<!-- .element: class="fragment highlight-red" data-fragment-index="2" --></span> a patent for main markets

(EU,    GB,    CN,    JP,    US)

=

### <span>30.000…100.000+<!-- .element: class="fragment" data-fragment-index="1" --></span> €

`…per idea to protect.`

<span>Make sure you need it.<!-- .element: class="fragment" data-fragment-index="3" --></span>


<!--- source:
[german] https://www.patent-pilot.com/de/ein-patent-anmelden/kosten-der-patentanmeldung/
-->

Note:
    - Patents work nationally, so file one in every region
    - [Q] guess the costs

---

using a patent

in trial

=

### 100.000…30.000.000 € 

<span>Can you afford using your patent(s)?<!-- .element: class="fragment" data-fragment-index="1" --></span>

<!--- source:
https://preubohlig.de/wp-content/uploads/2019/07/PatentLitigationHoppe.pdf
https://apnews.com/press-release/news-direct-corporation/a5dd5a7d415e7bae6878c87656e90112
-->

Note:
    avg=3.6m€
    similar in the US

---

Once filed, the application procedure
takes <span>3…5 years<!-- .element: class="fragment" data-fragment-index="1" --></span> (in the EU).

<span>How long is your product lifespan?<!-- .element: class="fragment" data-fragment-index="2" --></span>

Note:
    [Q] How long does the process take?
    lifespan of consumer products: ~18 months

<!--- source:
https://www.epo.org/service-support/faq/own-file.html
-->

---

## But:

"Patents give you <span>a guarantee in trial."<!-- .element: class="fragment fade-out" data-fragment-index="1" --></span>

<span>a seat at the table, both offensively and defensively. That’s it."<!-- .element: class="fragment" data-fragment-index="1" --></span>
    
([Forbes](https://www.forbes.com/sites/stephenkey/2017/11/13/in-todays-market-do-patents-even-matter/?sh=2edbefab56f3))


Note:
    Unfortunately that's not the case.
    value (and legitimacy) of a patent shows off in trail
    **[Q]** What's offensive/defensive use?

---

# Q&A
---

any questions/discussion points?

<span>--<!-- .element: class="fragment" data-fragment-index="1" --></span>

<span>Remember the scracity story (virus mutants)?<!-- .element: class="fragment" data-fragment-index="1" --></span>

<span>When do patents make sense?<!-- .element: class="fragment" data-fragment-index="1" --></span>
<span>→ What do you think?<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

## IP Law in a Table

|  | Copyright | Patent | Trademark |
| -------- | -------- | -------- | ----- |
| for | creative act | invention | identity |
| applies | by default | when filed | when registered |
| costs | - | €€€ | € |
| lasts | ~100 years | ≤20 years | as long as paid |

---

Bypassing the broken patent system
and maximising impact of your invention

after a **10min**

# Bio Break

---

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

## What is Open Source?

**= knowledge shared**

- <span>to everyone<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>forever<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>everywhere<!-- .element: class="fragment" data-fragment-index="3" --></span>

Note:
    Well this sounds much like Wikipedia, right?

---

![](https://upload.wikimedia.org/wikipedia/commons/8/80/Wikipedia-logo-v2.svg =400x400)

Note:
    - Wikipedia is an open source encyclopaedia which outcompeted all these super-expensive printed versions - still remember them?
    - As content is open source there, everyone can make use of it as he/she likes. You could even make a collection of your favourite articles, print and sell them as a book.
    - Also it's software base - Wikimedia - is open source.
    [Q] Do you have some everyday example for open source?

---

![image alt](https://upload.wikimedia.org/wikipedia/commons/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg "Pizza" =400x400) ![image alt](https://upload.wikimedia.org/wikipedia/commons/d/d2/Pythagorean.svg "Pizza" =400x400)

Note:
    - Everyone knows how to make Pizza, there's no copyright on the recipe.
    - Same for the Theorem of Pythagoras - large parts of our educational (and socio-economic) system wouldn't work if _everything_ would be proprietary.
    - The same applies for our IT infrastructure →

---

<span>![](https://upload.wikimedia.org/wikipedia/commons/3/3a/Tux_Mono.svg =214x258)<!-- .element: class="fragment" data-fragment-index="1" --></span>

|       |                      | 
| ----- | -------------------- | 
| 100 % | supercomputers       | 
| 95 %  | servers (top 1 Mio.) |
| 85 %  | mobile devices       |
| 70 %  | embedded systems     |

<!---
sources:
https://itsfoss.com/linux-runs-top-supercomputers/ bzw. https://www.top500.org/statistics/details/osfam/1/
https://www.zdnet.com/article/can-the-internet-exist-without-linux/
https://www.embedded.com/wp-content/uploads/2019/11/EETimes_Embedded_2019_Embedded_Markets_Study.pdf
https://gs.statcounter.com/os-market-share/mobile/worldwide
-->

Note:
    - You probably heard of the largest software project of human history, the kernel of the by far most used operating system, powering xx % of all xx.
    - Linux.

---

<span>Proprietary software
    contains **40…60%** free/open source code.<!-- .element: class="fragment" data-fragment-index="1" --></span>
<div></div>
<span>No one writes software from scratch today.<!-- .element: class="fragment" data-fragment-index="2" --></span>
<div></div>
<span>The same can/will happen for hardware.<!-- .element: class="fragment" data-fragment-index="3" --></span>

<!---
sources:

Flexera Report „State of Open Source License Compliance 2020“
https://www.zdnet.com/article/60-percent-of-codebases-contain-open-source-vulnerabilities/
https://www.helpnetsecurity.com/2018/05/22/open-source-code-security-risk/
-->

Note:
    And not only that:
    [Q] What do you think is the average percentage of open source code in proprietary code?

---

## What is open source ++hardware++?

<span>"[…] hardware whose design is made publicly available so that anyone can 
    study, modify, distribute, 
    make, and sell 
    the design or hardware based on that design."<!-- .element: class="fragment" data-fragment-index="1" --></span>
<span>([OSHWA](https://www.oshwa.org/definition))<!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    - That's the definition from the Open Source Hardware Association.
    - So no dependencies.
    - You are publishing design files and just anyone could build and sell the hardware.

---

## Is business even possible then?

<span>Ever bought 
    a pizza?<!-- .element: class="fragment" data-fragment-index="1" --></span>
<span>or screws?<!-- .element: class="fragment" data-fragment-index="2" --></span>
<span>or embedded systems running linux?<!-- .element: class="fragment" data-fragment-index="3" --></span>
<span>![image alt](https://upload.wikimedia.org/wikipedia/commons/3/34/Linksys-Wireless-G-Router.jpg =320x300)<!-- .element: class="fragment" data-fragment-index="3" --></span>

---

## Some businesses are only possible ++because++ they are (partly) open.

---

# Group Action
---

[30min]

### Unlock your Concept

- Where does open source make sense?
- What exactly will you be selling?
- <span>Pitch by the end of the class.<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

## Open Source Hardware
## in practice
---

---

## [Arduino](https://www.arduino.cc/)

<iframe width="1024" height="576" data-src="https://www.youtube.com/embed/UoBUXOOdLXY?start=156" allowfullscreen data-autoplay></iframe>

<!--- list of perfect ratios https://antifreezedesign.wordpress.com/2011/05/13/permutations-of-1920x1080-for-perfect-scaling-at-1-77/ -->

---

<!-- .slide: data-background="https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/EggPainter.png" data-background-color="#000" -->

## [Eggprinter](https://www.thingiverse.com/thing:2245428)

---

<!-- .slide: data-background="https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/AssemblyInstructions/Assembly.png" data-background-color="#000" -->

<span>![](https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/Samples/More%20Eggbot%20Op%20Art/preview.jpg)<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

<!-- .slide: data-background="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/Safecast_bGeigie_Nano_opened.jpg" data-background-color="#000" -->

## [Safecast](https://safecast.org/)

![image alt](https://upload.wikimedia.org/wikipedia/en/7/77/Safecast_Tile_Map_screenshot.jpeg) Fukushima crisis response

---

<!-- .slide: data-background="https://i2.wp.com/fossa.systems/wp-content/uploads/2019/10/66748130_891630314507139_3361097979711717376_n.jpg" data-background-color="#000" -->

## [FOSSASAT](https://fossa.systems/fossasat-1/)

---

<!-- .slide: data-background="https://i.ytimg.com/vi/gXP01bjIwHo/maxresdefault.jpg" data-background-color="#000" -->

## [LifeTrac](https://wiki.opensourceecology.org/wiki/LifeTrac_6)

Open Source Ecology (US)

---

<!-- .slide: data-background="https://icdn3.digitaltrends.com/image/digitaltrends/farmbot-express-feat-3251132-2189x1459.jpg" data-background-color="#000" -->

## [Farmbot](https://farm.bot/)

---

<!-- .slide: data-background="https://images.ctfassets.net/mu8m5cabjuvl/41OKPH0EKvv3HtT9bl6fPU/d232e447784bb70e972ab9430df58e0a/Plastic_Products.jpg" data-background-color="#000" -->

## [Precious Plastic](https://preciousplastic.com/)

---

## OSH <span><!-- .element: class="fragment highlight-red" -->≠</span> DIY

Note:
    - Even though lots of OSH projects are designed to be replicated by anyone, I want to point out that OSH IS NOT DIY
    - DIY:
      - limited to what you can do yourself
      - often unclear license
      - commercial use not considered

---

| enterprise | annual revenue |
| ---------- | -------------- |
| Arduino    | `$` 161.9m     |
| Sparkfun   | `$` 72.6m      |
| Prusa      | `$` 50.2m      |
| …          |                |

<!---

sources: 

https://ecommercedb.com/en/store/arduino.cc
https://ecommercedb.com/en/store/sparkfun.com
https://www.wikidata.org/wiki/Q27923775

-->

---

<!-- .slide: data-background="https://3s81si1s5ygj3mzby34dq6qf-wpengine.netdna-ssl.com/wp-content/uploads/2020/05/ab_facebook-ocp-racks-server.jpg" data-background-color="#000" -->

## [Open Compute Project](https://www.opencompute.org/)

---

<!-- .slide: data-background="https://upload.wikimedia.org/wikipedia/commons/d/db/Reprap_Darwin_2.jpg" data-background-color="#000" -->

## [RepRap](https://reprap.org/)

---

<iframe width="1024" height="576" data-src="https://www.opensourceimaging.org/"></iframe>

---

## Open Market Effects

### <span>1. Impact<!-- .element: class="fragment" data-fragment-index="1" --></span>

<span>also…<!-- .element: class="fragment" data-fragment-index="2" --></span>

- <span>market penetration (Android)<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>free supply chains (CERN)<!-- .element: class="fragment" data-fragment-index="3" --></span>
- <span>community (headhunting)<!-- .element: class="fragment" data-fragment-index="4" --></span>

Note:
    Why would people do that?
    Intrinsic factor: Impact (first & foremost)
    Extrinsic factors: market penetration…
    But of course there are challenges →

---

## Business challenges:

- <span>less dependencies<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>experimental path<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>new tools, tasks and legal issues<!-- .element: class="fragment" data-fragment-index="3" --></span>

Note: 
    - dependencies are a solid base for commercial business models
    - sacrifice a fairly predictable path
    - …for new tools, tasks and legal issues


---

## Why bother?

Note:
    There are many anwers to this question.
    For me they all boil down to a self-reflection.
    In one phrase:

---

## What's the point of your business?

<!---
There are ways to use open source just for commercial business. Google's Android showed how well market penetration works using open source.
--->

---

> “Much as Wikipedia has sought to democratize access to knowledge 
> and the open source software movement has attempted to democratize computing,
> Open Source Ecology seeks to democratize human wellbeing
> and the industrial tools
> that help to create it.”
([Open Source Ecology](https://www.mitpressjournals.org/doi/pdf/10.1162/INOV_a_00139))

---

# Q&A
---

---

Hands–on Session :tada:

after a **20min**

# Coffee Break

---

### Hands-on Plan
---

0. <span>How to Open the Source<!-- .element: class="fragment" data-fragment-index="1" --></span>
1. Come back to your ideas
2. Technology research
3. Openness check

---

## What makes HW open?

remember OSHWA?
([DIN SPEC 3105-1](https://gitlab.com/OSEGermany/OHS/-/releases/0.10.0))

- <span>**Documentation**<!-- .element: class="fragment" data-fragment-index="1" --></span> <span>must be:<!-- .element: class="fragment" data-fragment-index="1" --></span>
  - <span>complete<!-- .element: class="fragment" data-fragment-index="2" --></span>
  - <span>editable<!-- .element: class="fragment" data-fragment-index="3" --></span>
  - <span>accessible<!-- .element: class="fragment" data-fragment-index="4" --></span>

- <span>free/open<!-- .element: class="fragment" data-fragment-index="5" --></span> <span>**LICENSE**<!-- .element: class="fragment" data-fragment-index="5" --></span>

---

Lazy man's guide to licensing:

---

## Copyright & Copyleft

---

### <span>Copyright<!-- .element: class="fragment fade-out" data-fragment-index="1" --></span> <span>free/open source<!-- .element: class="fragment fade-in-then-out" data-fragment-index="1" --></span> <span>Copyleft<!-- .element: class="fragment " data-fragment-index="2" --></span>
---

<span>exclusive right to use, modify and to make copies<!-- .element: class="fragment fade-out" data-fragment-index="1" --></span>

<span>everyone can use, modify and make copies<!-- .element: class="fragment" data-fragment-index="1" --></span> <span>of all copies and adoptions<!-- .element: class="fragment" data-fragment-index="2" --></span>

---

## 3 flavours of copyleft

|                |                                       |
| -------------- | ------------------------------------- |
| strong         | _everything_ must be free/open        |
| weak           | only direct changes must be free/open |
| non/permissive | proprietary adoptions allowed         |

---

## Popular licenses

| copyleft       | <i class="fa fa-creative-commons"></i> | <i class="fa fa-code"></i> | <i class="fa fa-cog"></i> |
| -------------- | ---- | ---- | ---- |
| strong         | [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode) | [GPLv3]() | [CERN OHL-S]() |
| weak           | - | [LGPLv3]() | [CERN OHL-W]() |
| non/permissive | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) | [Apache 2.0]() | [CERN OHL-P]() |

---

## Is there a list?

<span>…of course:<!-- .element: class="fragment" data-fragment-index="1" --></span> <span>[SPDX](https://spdx.org/licenses/)<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

### Now it's just a matter of publishing:

- [x] bill of materials
- [x] design files
  - native format
    (FreeCAD files, Gerber files,…)
  - export format
    (STP, STL, PDF,…)
- [x] rationale
- [x] assembly instructions / drawing
- [x] user manual

---

# Group Action
---

[40min]

### Technology Research

- Search for existent solutions.
- Someone's already doing it? → :tada:
    Fork it and contact the team
    Lots of work already done!

---

Where to find OSH?

The internet is messy <span>(yet)<!-- .element: class="fragment" data-fragment-index="1" --></span>.

<span>Find a list of platforms and useful resources<!-- .element: class="fragment" data-fragment-index="1" --></span>
<span>[here](https://github.com/OPEN-NEXT/LOSH-list/blob/main/platform-list.md)<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

# Group Action
---

[15min]

### Openness Check

- free/open license? ([SPDX](https://spdx.org/licenses/))
  - non-commercial ≠ open source!
  - non-derivative ≠ open source!
- crucial documentation published?
  - bill of materials
  - original design files

---

Shine at your pitch :sparkles: 

after a **10min**

# Bio Break

---

# Pitch
---

[3min each]

- What are (new) open source assets in your concept?
  - (New) communities to connect with?
- What exactly will you be selling?

---

# Outro
---

Great Ideas!

Keep it up!

Contribute to a better world for everyone!
(it's kinda urgent)

---

Our next session (11.5.) will be designed by ++you++

Questions? → file an issue [here](https://gitlab.com/osh-academy/oer-osh-intro/-/issues)

↓

- tag me for response (@moedn)
- (:+1:) vote for issues
    → top questions will make the agenda

---

Bonus:

## The basis for a circular economy

---

## What if…

Companies would accept the return
of all products they ever sold so
they would recycle them?

Note:
    [Discussion]

---

### …we'd get zillions of parallel individual loops.

([talk](https://youtu.be/dJ8DIn2vEV0))

<span>**How about connecting them?**<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

## That's open source hardware!

If we publish the <span><!-- .element: class="fragment highlight-red" data-fragment-index="1"-->complete documentation</span>
under a <span><!-- .element: class="fragment highlight-red" data-fragment-index="1"-->free/open license</span>, anyone is able to
locally study, produce, maintain, modify […]
the hardware.

Note:
    And that's happening! →

---

## Community, community, …

We, the OSH community, are building
a circular, open society,
"democratising human wellbeing".
We construct rather than complain.

### <span>Now, who's "we"?<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

<!-- .slide: data-transition="zoom" -->

## We is you.

---

## What do ++you++ want to see as

## open source?
